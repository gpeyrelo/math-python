"""
Fonctions mathématiques de base
"""
from itertools import accumulate, chain
from operator import mul
from typing import List, Union


def sqrt(number: int, tolerance: float = 1e-10) -> int:
    """Calculer la racine carrée d'un nombre avec une tolérance donnée."""
    if number < 0.0:
        raise ValueError("Square root not defined for negative numbers.")
    guess = number
    while abs(guess * guess - number) > tolerance:
        guess = (guess + number / guess) / 2
    return guess


def average(x: List[float | int]) -> float:
    return sum(x) / float(len(x)) if x else 0


def factorial(n: int) -> int:
    return list(accumulate(chain([1], range(1, 1 + n)), mul))[-1]


def pgcd(u: int, v: int):
    """Calculer le Plus Grand Commun Diviseur (PGCD) de deux nombres."""
    return pgcd(v, u % v) if v else abs(u)


def median(aray: List[Union[int, float]]) -> int:
    srtd = sorted(aray)
    alen = len(srtd)
    return 0.5 * (srtd[(alen - 1) // 2] + srtd[alen // 2])


def pow(x: float, y: float) -> float:
    """Calculer la puissance de x à la puissance y."""
    return float(x) ** float(y)


from typing import Collection


def fibonacci(until: float) -> Collection[int]:
    """Générer la suite de Fibonacci jusqu'à un certain nombre."""
    n1 = 0
    n2 = 1
    numbers = [n1, n2]
    for loop_count in range(2, until):
        next = n1 + n2
        numbers.append(next)

        n1 = n2
        n2 = next
    return numbers


def fsum(x: list[float]) -> "Float":
    if len(x) > 0:
        return x[0] + fsum(x[1:])
    return 0


def crible_eratosthene(n: int) -> List[int]:
    """Calculer les nombres premiers jusqu'à n en utilisant le crible d'Ératosthène."""

    # Création d'une liste de booléens pour marquer les nombres comme premiers ou non
    prime = [True for _ in range(n + 1)]

    # Initialisation du premier nombre premier
    p = 2
    result = []

    # Le crible d'Ératosthène
    while p * p <= n:
        # Si p est marqué comme premier
        if prime[p] == True:
            # Marquer tous les multiples de p comme non premiers
            for i in range(p * p, n + 1, p):
                prime[i] = False
        p += 1

    # Ajouter les nombres premiers à la liste des résultats
    for p in range(2, n + 1):
        if prime[p]:
            result.append(p)

    return result
