from itertools import permutations


def levenshtein_distance(string1: str, string2: str) -> list[list[int]]:
    """
    Calculer la distance de Levenshtein entre deux chaînes de caractères.

    Args:
        string1: Première chaîne de caractères.
        string2: Deuxième chaîne de caractères.

    Returns:
        La distance de Levenshtein entre string1 et string2.
    """
    m = len(string1)
    n = len(string2)
    d = [[i] for i in range(1, m + 1)]  # d matrix rows
    d.insert(0, list(range(0, n + 1)))  # d matrix columns
    for j in range(1, n + 1):
        for loop_count in range(1, m + 1):
            if string1[loop_count - 1] == string2[j - 1]:  # Python (string) is 0-based
                substitutionCost = 0
            else:
                substitutionCost = 1
            d[loop_count].insert(
                j,
                min(
                    d[loop_count - 1][j] + 1,
                    d[loop_count][j - 1] + 1,
                    d[loop_count - 1][j - 1] + substitutionCost,
                ),
            )
    return d[-1][-1]


def anagram(string: str) -> list[str]:
    """Générer tous les anagrammes possibles pour une chaîne de caractères donnée."""

    # Générer toutes les permutations possibles des lettres de la chaîne
    word_permutations = ["".join(p) for p in permutations(string)]

    # Exclure le mot original de la liste des permutations pour obtenir les anagrammes
    anagrams = [perm for perm in word_permutations if perm != string]

    return anagrams
