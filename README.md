## Equipe : LAVERGNE Guillaume (pour l'exercice 2)

Créez un patch à l’aide de git format-patch pour soumettre votre correction sous
forme de fichier. Regardez ce fichier, à quoi correspond-il ?

Le fichier patch est un fichier text qui contient les différences entre l'état actuel du dépôt et l'état au moment où le patch a été généré.
