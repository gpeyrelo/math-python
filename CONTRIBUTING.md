# Guide du contributeur pour le projet "math-python"

Ce guide du contributeur fournit des informations et des directives pour le projet "math-python".

## Comment contribuer

1. Consultez les issues ouvertes pour trouver des tâches à travailler.
2. Créez une branche pour travailler sur votre partie.
3. Effectuez les modifications nécessaires.
4. Assurez-vous que votre code respecte les conventions de style et de formatage du projet.
5. Soumettez une Merge Request en décrivant clairement les changements effectués et les problèmes résolus.

## Normes de codage

### Conventions de nommage

- Utilisez le style snake_case pour les noms de variables, fonctions et méthodes.
- Nommage explicite et significatif pour améliorer la lisibilité du code.

### Typage

- Utilisez les type hints pour annoter les fonctions et les variables.

## Documentation

- Les commentaires doivent expliquer les sections complexes du code ou le code difficile à comprendre.

## Tests

- Écrivez des tests unitaires appropriés pour toutes les nouvelles fonctionnalités ou modifications apportées.
- Assurez-vous que les tests existants ne sont pas cassés par vos modifications.

## Projet

En soumettant votre contribution, vous acceptez que votre code soit public.

Merci pour votre contribution à "math-python" !
