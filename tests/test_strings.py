from unittest import TestCase

from math2.strings import anagram, levenshtein_distance


class TestString(TestCase):
    def test_levenshtein(self):
        self.assertEqual(3, levenshtein_distance("kitten", "sitting"))
        self.assertEqual(8, levenshtein_distance("rosettacode", "raisethysword"))

    def test_anagram(self):
        word = "bat"
        expected_anagrams = ["bta", "abt", "atb", "tba", "tab"]
        result_anagrams = anagram(word)
        self.assertEqual(expected_anagrams, result_anagrams)
