"""
Test module for functions
"""
from unittest import TestCase

from math2.functions import (
    average,
    crible_eratosthene,
    factorial,
    fibonacci,
    fsum,
    median,
    pgcd,
    sqrt,
)


class TestFunctions(TestCase):
    def test_sqrt(self):
        self.assertEqual(5, sqrt(25))

    def test_sqrt_invalid_because_lower_than_zero(self):
        with self.assertRaises(ValueError):
            sqrt(-2)

    def test_average(self):
        self.assertEqual(2.3, average([0, 0, 3, 1, 4, 1, 5, 9, 0, 0]))
        self.assertEqual(1e-21, average([1e20, -1e-20, 3, 1, 4, 1, 5, 9, -1e20, 1e-20]))

    def test_factorial(self):
        self.assertEqual(8841761993739701954543616000000, factorial(29))

    def test_pgcd(self):
        self.assertEqual(34, pgcd(40902, 24140))

    def test_pow(self):
        from math2.functions import pow

        self.assertEqual(8388608, pow(2, 23))

    def test_median(self):
        self.assertEqual(4.4, median([4.1, 5.6, 7.2, 1.7, 9.3, 4.4, 3.2]))

    def test_fibonacci(self):
        self.assertEqual([0, 1, 1, 2, 3, 5, 8, 13, 21, 34], fibonacci(10))

    def test_fsum(self):
        self.assertEqual(9.25, fsum([2.0, 5, 0.25, 0.5, 0.33, 0.67, 0.5]))

    def test_crible_eratosthene(self):
        self.assertEqual([2, 3, 5, 7, 11, 13, 17, 19, 23, 29], crible_eratosthene(30))
